include build-system/config.mk

.SILENT: all
all: dist
	$(echo) "This is a python project, no compilation at this step required."
	$(echo) "Run make install to install global."
	$(echo) "Run make install-user to install for the current user."

# you need SOURCE_DATE_EPOCH at least at the 1980s when
# your default SOURCE_DATE_EPOCH in env is 1.  More information
# is found here: https://reproducible-builds.org/docs/source-date-epoch/
#
.PHONY: dist
dist: gana
	$(env) SOURCE_DATE_EPOCH=315532800 $(python) setup.py sdist bdist_wheel

.PHONY: gana
gana:
	cd gana/gnu-taler-error-codes/; make; cp taler_error_codes.py ../../taler/util/; cd ../..

install: gana all
	(cd dist; $(env) SOURCE_DATE_EPOCH=315532800 $(python) -m pip install --upgrade --no-index --find-links=. taler-util)


uninstall:
	(cd dist; $(env) SOURCE_DATE_EPOCH=315532800 $(python) -m pip uninstall taler-util)

install-user:
	(cd dist; $(env) SOURCE_DATE_EPOCH=315532800 $(python) -m pip install --user --no-index --find-links=. taler-util)

uninstall-user:
	(cd dist; $(env) SOURCE_DATE_EPOCH=315532800 $(python) -m pip uninstall taler-util)

pypi: dist
	$(env) SOURCE_DATE_EPOCH=315532800 $(python) -m twine upload dist/*

check:
	$(tox) || echo "error: you have to install tox"

pretty:
	black tests/ taler/

clean:
	$(rm) -rf __pycache__ *~
